import React from "react";
import "./About.css";
import logo from "../logo.svg";
const About = () => {
  return (
    <div className="container">

      <div className="py-4">

        <h1>About Page</h1>
        

<img src={logo} className="App-logo" alt="logo" />
        <p className="lead">

  React is a declarative, efficient, and flexible JavaScript
  library for building user interfaces. It lets you compose
  complex UIs from small and isolated pieces of code called 
  components”.We'll get to the funny XML-like tags soon. 
  We use components to tell React what we want to see on the
  screen.
 
React makes it painless to create interactive UIs. 
Design simple views for each state in your application,
 and React will efficiently update and render just the
  right components when your data changes.

Declarative views make your code more predictable and
 easier to debug.

Component-Based
Build encapsulated components that manage their own state,
 then compose them to make complex UIs.

Since component logic is written in JavaScript instead 
of templates, you can easily pass rich data through your
 app and keep state out of the DOM.

Learn Once, Write Anywhere
We don’t make assumptions about the rest of your
 technology stack, so you can develop new features
  in React without rewriting existing code.

React can also render on the server using Node and
 power mobile apps using React Native.
        </p>
        {/* <p className="lead">
          Lorem ipsum, dolor sit amet consectetur adipisicing elit. Cumque rerum
          hic ab veniam reiciendis cum repudiandae, voluptate explicabo nesciunt
          nam accusantium? Soluta cupiditate, accusamus commodi praesentium
          laborum dolorum libero maiores!
        </p>
        <p className="lead">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Cumque rerum hic ab veniam reiciendis cum repudiandae, voluptate explicabo nesciunt nam accusantium? Soluta cupiditate, accusamus commodi praesentium laborum dolorum libero maiores!</p> */}
      </div> 
    </div>
  );
};

export default About;


