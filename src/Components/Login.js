import React, { Component } from 'react';
import './Login.css';
// import { Link } from "react-router-dom";

import {Button,Form,FormGroup,Label,Input} from 'reactstrap';
import { FacebookLoginButton} from 'react-social-login-buttons';
// import { GoogleLoginButton} from 'react-social-login-buttons';
class Login extends Component {
    render() {
        return (
            // <div>
            //  <Link className="btn btn-primary" to="/">
            //        Back to Home
            //        </Link>       
            // </div>
                <Form className="login-form">    
                 <h2 className="font">
                 <span className="font-weight-bold">Login Page</span>
                 </h2> 
                 <FormGroup>
                 {/* <label>UserName</label> */}
                 <input type="UserName" placeholder="UserName"/>
                 </FormGroup>
                  <FormGroup>
                 {/* <label > Password</label>  */}

                 <input type="Password" placeholder="Password"/>
                 </FormGroup>
                     {/* <Button className="btn-lg btn-dark btn-block">
                     Log In
                     </Button> */}

                     <Button className="button">
                     Log In
                     </Button>
                     <div className="text-center pt-3">
                     or continue with your social account
                     </div>
                     <FacebookLoginButton  className="mt-3 mb-3"/>

                     {/* <GoogleLoginButton className="mt-4 mb-4"/> */}
                    <div className="text-center">
                    <a href="/sign-up">Sign Up</a>
                    <span className="p-2">|</span>
                    <a href="/Forgot Password">Forgot password</a>
                     </div>
                  </Form> 
            )
      }
}
export default Login;